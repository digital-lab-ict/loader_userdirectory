curl 'http://wl-wsserver.eni.it:8080/up-srvc-aim/services/getMetaFileFull' -o report_all.csv.zip
unzip report_all.csv.zip
if [ -f ./report_all.csv ]; then
  curl http://api-userdirectory.api-net:8983/solr/user_directory/update?commit=true -H "Content-Type: text/xml" --data-binary '<delete><query>*:*</query></delete>'
  curl -X POST 'http://api-userdirectory.api-net:8983/solr/user_directory/schema' --data-binary @user_directory_schema.json -H 'Content-type:application/json'
  python prepare.py
  find . -name 'report_s_*' | while read split_file_name; do
    curl 'http://api-userdirectory.api-net:8983/solr/user_directory/update?commit=true&trim=true&header=true&separator=%3B' --data-binary @$split_file_name -H 'Content-type:application/csv'
    rm $split_file_name
    done;
  rm report_all.csv.zip
  rm report_all.csv
fi
