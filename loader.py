import os
import time
import datetime

def scheduler():
    start_time = os.environ["START_TIME"]
    start_time = datetime.datetime.strptime(start_time, '%H:%M')
    print "START_TIME: " + str(start_time)
    while True:
        now = datetime.datetime.now()
        td = (now - start_time).seconds / 60
        if td >= 0 and td < 60:
            os.system('bash ./init_user_directory.sh')
            time.sleep(3600)
        time.sleep(60)

if __name__ == "__main__":
    scheduler()
